/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Camion;
import ec.edu.espol.model.Camioneta;
import ec.edu.espol.model.Carro;
import ec.edu.espol.model.Motocicleta;
import ec.edu.espol.model.Vehiculo;
import ec.edu.espol.model.Vendedor;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;

/**
 * FXML Controller class
 *
 * @author jarvi
 */
public class IngresoVehículoFXMLController implements Initializable {

    /**
     * Initializes the controller class.
     */
    String tipo;
    
    String texto = null;
    

    @FXML
    private ComboBox cbxTipos;

    @FXML
    private VBox vPane1;

    @FXML
    private VBox vPane2;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ArrayList<String> tipos = new ArrayList<>();
        tipos.add("auto");
        tipos.add("camioneta");
        tipos.add("motocicleta");
        tipos.add("camión");
        cbxTipos.setItems(FXCollections.observableArrayList(tipos));
    }

    @FXML
    private void cambiarCombo(ActionEvent e) {
        ComboBox cb = (ComboBox) e.getSource();
        tipo = (String) cb.getValue();
        vPane1.getChildren().clear();
        vPane2.getChildren().clear();
        vPane1.setSpacing(5);
        vPane2.setSpacing(5);
        textFields("INGRESE LA PLACA", vPane1);
        textFields("INGRESE EL MODELO", vPane1);
        textFields("INGRESE EL TIPO DE MOTOR", vPane1);
        TextField recorrido = textFields("INGRESE EL RECORRIDO", vPane1);
        recorrido.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("[0-9.]*")) ? change : null));
        textFields("INGRESE EL COLOR", vPane1);
        TextField precio = textFields("INGRESE EL PRECIO", vPane1);
        precio.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("[0-9.]*")) ? change : null));
        TextField anio = textFields("INGRESE EL AÑO", vPane1);
        anio.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("[0-9]*")) ? change : null));

        if (tipo.equals("auto")) {
            TextField vidrios = textFields("INGRESE EL NÚMERO DE VIDRIOS", vPane2);
            vidrios.setTextFormatter(new TextFormatter<>(change
                    -> (change.getControlNewText().matches("[0-9]*")) ? change : null));
            textFields("INGRESE LA LA TRANSMISIÓN", vPane2);
            Text imagen = new Text("SELECCIONE LA IMAGEN DEL AUTO");
            vPane2.getChildren().add(imagen);
            cargarImagen();
        } else if (tipo.equals("camioneta")) {
            TextField vidrios = textFields("INGRESE EL NÚMERO DE VIDRIOS", vPane2);
            vidrios.setTextFormatter(new TextFormatter<>(change
                    -> (change.getControlNewText().matches("[0-9]*")) ? change : null));
            textFields("INGRESE LA LA TRANSMISIÓN", vPane2);
            textFields("INGRESE LA LA TRACCIÓN", vPane2);
            Text imagen = new Text("SELECCIONE LA IMAGEN DE LA CAMIONETA");
            vPane2.getChildren().add(imagen);
            cargarImagen();
        } else if (tipo.equals("motocicleta")) {
            textFields("INGRESE EL TIPO DE ASIENTO", vPane2);
            textFields("INGRESE EL TIPO DE CADENA", vPane2);
            Text imagen = new Text("SELECCIONE LA IMAGEN DE LA MOTOCICLETA");
            vPane2.getChildren().add(imagen);
            cargarImagen();
        } else if (tipo.equals("camión")) {
            TextField vidrios = textFields("INGRESE EL NÚMERO DE VIDRIOS", vPane2);
            vidrios.setTextFormatter(new TextFormatter<>(change
                    -> (change.getControlNewText().matches("[0-9]*")) ? change : null));
            textFields("INGRESE LA LA TRANSMISIÓN", vPane2);
            Text imagen = new Text("SELECCIONE LA IMAGEN DEL CAMIÓN");
            vPane2.getChildren().add(imagen);
            cargarImagen();
        }

    }

    @FXML
    private void ingresar(ActionEvent event) {
        
        if (vPane1.getChildren().isEmpty()){
            Alert a = new Alert(Alert.AlertType.WARNING,"Usted no ha ingresado ningún Vehículo");
            a.show();
        }
        else{
            ArrayList<Vehiculo> vehiculos = Vehiculo.deserializarVehiculos("vehiculos.ser");
            TextField placa = (TextField) vPane1.getChildren().get(1);
            TextField modelo = (TextField) vPane1.getChildren().get(3);
            TextField motor = (TextField) vPane1.getChildren().get(5);
            TextField recorrido = (TextField) vPane1.getChildren().get(7);
            TextField color = (TextField) vPane1.getChildren().get(9);
            TextField precio = (TextField) vPane1.getChildren().get(11);
            TextField anio = (TextField) vPane1.getChildren().get(13);

            if (tipo.equals("auto")) {
                TextField vidrios = (TextField) vPane2.getChildren().get(1);
                TextField transmision = (TextField) vPane2.getChildren().get(3);

                if (placa.getText().equals("") || modelo.getText().equals("") || motor.getText().equals("")
                        || recorrido.getText().equals("") || color.getText().equals("") || precio.getText().equals("")
                        || anio.getText().equals("") || vidrios.getText().equals("") || transmision.getText().equals("")) {
                    Alert a = new Alert(Alert.AlertType.WARNING, "No deje Información en blanco");
                    a.show();
                }
                   
               boolean bool = verificarPlaca(vehiculos, placa.getText());

                if (bool == false) {
                    Carro auto = new Carro(placa.getText(), modelo.getText(), motor.getText(),
                            Double.parseDouble(recorrido.getText()), color.getText(),
                            Double.parseDouble(precio.getText()), tipo, Integer.parseInt(vidrios.getText()),
                            transmision.getText(), Integer.parseInt(anio.getText()));
                    ingresarVehículo(vehiculos, auto, "vehiculos.ser", "Auto Ingresado");
//                    guardarImagen(texto);
                    cambiarRoot();
                }
                
            } else if (tipo.equals("camioneta")) {
                TextField vidrios = (TextField) vPane2.getChildren().get(1);
                TextField transmision = (TextField) vPane2.getChildren().get(3);
                TextField traccion = (TextField) vPane2.getChildren().get(5);

                if (placa.getText().equals("") || modelo.getText().equals("") || motor.getText().equals("")
                        || recorrido.getText().equals("") || color.getText().equals("") || precio.getText().equals("")
                        || anio.getText().equals("") || vidrios.getText().equals("") || transmision.getText().equals("") || traccion.getText().equals("")) {
                    Alert a = new Alert(Alert.AlertType.WARNING, "No deje Información en blanco");
                    a.show();
                }

                boolean bool = verificarPlaca(vehiculos, placa.getText());

                if (bool == false) {
                    Camioneta camioneta = new Camioneta(placa.getText(), modelo.getText(), motor.getText(),
                            Double.parseDouble(recorrido.getText()), color.getText(),
                            Double.parseDouble(precio.getText()), tipo, Integer.parseInt(vidrios.getText()),
                            traccion.getText(), transmision.getText(), Integer.parseInt(anio.getText()));
                    ingresarVehículo(vehiculos, camioneta, "vehiculos.ser", "Camioneta Ingresado");
                    cambiarRoot();
                }
            } else if (tipo.equals("motocicleta")) {
                TextField asiento = (TextField) vPane2.getChildren().get(1);
                TextField cadena = (TextField) vPane2.getChildren().get(3);

                if (placa.getText().equals("") || modelo.getText().equals("") || motor.getText().equals("")
                        || recorrido.getText().equals("") || color.getText().equals("") || precio.getText().equals("")
                        || anio.getText().equals("") || asiento.getText().equals("") || cadena.getText().equals("")) {
                    Alert a = new Alert(Alert.AlertType.WARNING, "No deje Información en blanco");
                    a.show();
                }
                
                else{
                    boolean bool = verificarPlaca(vehiculos, placa.getText());

                    if (bool == false) {
                        Motocicleta moto = new Motocicleta(placa.getText(), modelo.getText(), motor.getText(),
                                Double.parseDouble(recorrido.getText()), color.getText(),
                                Double.parseDouble(precio.getText()), tipo, asiento.getText(),
                                cadena.getText(), Integer.parseInt(anio.getText()));
                        ingresarVehículo(vehiculos, moto, "vehiculos.ser", "Motocicleta Ingresado");
                        cambiarRoot();
                    }
                }

                

            } else if (tipo.equals("camión")) {
                TextField vidrios = (TextField) vPane2.getChildren().get(1);
                TextField transmision = (TextField) vPane2.getChildren().get(3);

                if (placa.getText().equals("") || modelo.getText().equals("") || motor.getText().equals("")
                        || recorrido.getText().equals("") || color.getText().equals("") || precio.getText().equals("")
                        || anio.getText().equals("") || vidrios.getText().equals("") || transmision.getText().equals("")) {
                    Alert a = new Alert(Alert.AlertType.WARNING, "No deje Información en blanco");
                    a.show();
                }
                
                else{
                    boolean bool = verificarPlaca(vehiculos, placa.getText());

                    if (bool == false) {
                        Camion camion = new Camion(placa.getText(), modelo.getText(), motor.getText(),
                                Double.parseDouble(recorrido.getText()), color.getText(),
                                Double.parseDouble(precio.getText()), tipo, Integer.parseInt(vidrios.getText()),
                                transmision.getText(), Integer.parseInt(anio.getText()));
                        ingresarVehículo(vehiculos, camion, "vehiculos.ser", "Camion Ingresado");
                        cambiarRoot();
                }
            }
        }
        }
    }

    @FXML
    private void regresar(ActionEvent event) {
        try {
            App.setRoot("VentanaVendedorFXML");
        } catch (IOException ex) {
            Alert a = new Alert(Alert.AlertType.WARNING, "No se puede cambiara el root");
            a.show();
        }
    }

    private TextField textFields(String texto, VBox vpane) {
        Text text = new Text(texto);
        vpane.getChildren().add(text);
        TextField textF = new TextField();
        vpane.getChildren().add(textF);
        return textF;
    }
    
    private boolean verificarPlaca(ArrayList<Vehiculo> vehiculos, String placa){
        boolean bool = false;
        for (Vehiculo v : vehiculos) {
            if (v.getPlaca().equals(placa)) {
                bool = true;
                Alert a = new Alert(Alert.AlertType.ERROR, "La PLACA ya se encuentra registrada");
                a.show();
            }
        }
        return bool;
    }
    
    private void ingresarVehículo(ArrayList<Vehiculo> vehiculos, Vehiculo v1, String nomfile, String mensaje){
        vehiculos.add(v1);
        Vehiculo.serializarVehiculos(vehiculos, nomfile);
        Alert a = new Alert(Alert.AlertType.INFORMATION, mensaje);
        a.show();
    }
    
    private void cambiarRoot(){
        try {
            App.setRoot("VentanaVendedorFXML");
        } catch (IOException ex) {
            Alert a = new Alert(Alert.AlertType.WARNING,"No se puede cambiar el root");
            a.show();
        }
    }

    private void cargarImagen() {
        FileChooser fileChooser = new FileChooser();
        Button openButton = new Button("Open a Picture");
        vPane2.getChildren().add(openButton);
        VBox vPane3 = new VBox();
        vPane2.getChildren().add(vPane3);
        openButton.setOnAction(
                new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                fileChooser.setTitle("Buscar Imagen");
                // Filtros
                fileChooser.getExtensionFilters().addAll(
                        new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
                // Obtener y mostrar la imagen seleccionada
                File imgFile = fileChooser.showOpenDialog(null);                    
                if (imgFile != null) {
                    Image image = new Image("File:" + imgFile.getAbsolutePath());
                    Text urlImg = new Text(image.getUrl());
                    ImageView imgview = new ImageView();
                    imgview.setFitHeight(180);
                    imgview.setFitWidth(300);
                    imgview.setImage(image);
                    vPane3.getChildren().clear();
                    vPane3.getChildren().add(imgview);
                }
            }
        });
    }
    
    private void guardarImagen(String text){
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                        new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif")); //new FileChooser.ExtensionFilter("All Images", "*.*"),new FileChooser.ExtensionFilter("JPG", "*.jpg"),new FileChooser.ExtensionFilter("PNG", "*.png")
        File file = fileChooser.showSaveDialog(null);
        if(file!=null){
            try(FileWriter fw = new FileWriter(file, false); 
                    BufferedWriter bw = new BufferedWriter(fw)) {
                    bw.write(text, 0, text.length());
            } catch (Exception e) {
                    Alert a = new Alert(Alert.AlertType.ERROR, "Imagen no guardada");
                    a.show();
            }
        }
    }
    
    
    
}
