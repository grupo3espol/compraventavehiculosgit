/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Comprador;
import static ec.edu.espol.model.Comprador.comprobarExistencia;
import static ec.edu.espol.model.Comprador.deserializarCompradores;
import static ec.edu.espol.model.Comprador.serializarUsuarioComprador;
import ec.edu.espol.model.Vendedor;
import static ec.edu.espol.model.Vendedor.comprobarExistencia;
import static ec.edu.espol.model.Vendedor.deserializarVendedores;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
/**
 * FXML Controller class
 *
 * @author gerar
 */
public class InicioSesionFXMLController implements Initializable {


    @FXML
    private ComboBox cbxInicioSesion;
    @FXML
    private Button btnInicioSesion;
    @FXML
    private TextField ingresoUsuario;
    @FXML
    private TextField ingresoContra;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        ArrayList<String> lista = new ArrayList<>();
        lista.add("Comprador");
        lista.add("Vendedor");
        lista.add("Comprador y vendedor");
        cbxInicioSesion.setItems(FXCollections.observableArrayList(lista));
    }    
    
    @FXML
    private void iniciarSesion(MouseEvent event) {
        String valor;
        
        try{
            valor = (String)cbxInicioSesion.getValue();
            eleccionUsuario(valor, ingresoUsuario.getText(), ingresoContra.getText());
        } catch(Exception e) {
            e.getMessage();
        }
              
    }
    
    //Adicionales
    private static void eleccionUsuario(String valor, String ingresoUsuario, String ingresoContra) {
        ArrayList<Comprador> compradores = deserializarCompradores("listaCompradores.ser");
        ArrayList<Vendedor> vendedores = deserializarVendedores("listaVendedores.ser");
        
        switch (valor) {
            case "Comprador":
                Comprador comprador = comprobarExistencia(compradores, ingresoUsuario, ingresoContra);
                
                if(comprador != null) {
                    try {
                        Comprador.serializarUsuarioComprador(comprador, "listaCompradores.ser");
                        App.setRoot("VentanaCompradorFXML");
                    } catch (IOException ex) {
                        Alert a = new Alert(Alert.AlertType.ERROR, "No es posible cambiar de menú");
                    }
                }   break;
            case "Vendedor":
                Vendedor vendedor = comprobarExistencia(vendedores, ingresoUsuario, ingresoContra);
                if(vendedor != null) {
                    try {
                        Vendedor.serializarUsuarioVendedor(vendedor, "listaVendedores.ser");
                        App.setRoot("VentanaVendedorFXML");
                    } catch (IOException ex) {
                        Alert a = new Alert(Alert.AlertType.ERROR, "No es posible cambiar de menú");
                    }
                }   break;
            case "Comprador y vendedor":
                Comprador comprador1 = comprobarExistencia(compradores, ingresoUsuario, ingresoContra);
                Vendedor vendedor1 = comprobarExistencia(vendedores, ingresoUsuario, ingresoContra);
                if((comprador1 != null) && (vendedor1 != null)) {
                    try {
                        Comprador.serializarUsuarioComprador(comprador1, "listaCompradores.ser");
                        Vendedor.serializarUsuarioVendedor(vendedor1, "listaVendedores.ser");
                        App.setRoot("VentanaCompradorVendedorFXML");
                    } catch (IOException ex) {
                        Alert a = new Alert(Alert.AlertType.ERROR, "No es posible cambiar de menú");
                    }
                }   break;
            default:
                ;
                break;
        }
    }
}
