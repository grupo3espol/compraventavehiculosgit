/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Comprador;
import static ec.edu.espol.model.Comprador.deserializarCompradores;
import static ec.edu.espol.model.Comprador.modificarListaComprador;
import static ec.edu.espol.model.Comprador.serializarCompradores;
import static ec.edu.espol.model.Usuario.comprobarGmail;
import ec.edu.espol.model.Vendedor;
import static ec.edu.espol.model.Vendedor.deserializarVendedores;
import static ec.edu.espol.model.Vendedor.modificarListaVendedor;
import static ec.edu.espol.model.Vendedor.serializarVendedores;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author gerar
 */
public class RegistroFXMLController implements Initializable {

    @FXML
    private ComboBox cbxRegistro;
    @FXML
    private TextField fldNombre;
    @FXML
    private TextField fldApellido;
    @FXML
    private TextField fldCorreo;
    @FXML
    private TextField fldOrganizacion;
    @FXML
    private TextField fldUsuario;
    @FXML
    private TextField fldContra;
    @FXML
    private Button btnRegistrar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        ArrayList<String> lista = new ArrayList<>();
        lista.add("Comprador");
        lista.add("Vendedor");
        lista.add("Comprador y vendedor");
        cbxRegistro.setItems(FXCollections.observableArrayList(lista));
    }    

    @FXML
    private void registrarUsuario(MouseEvent event) {
        String valor;
        ArrayList<Comprador> compradores = deserializarCompradores("listaCompradores.ser");
        ArrayList<Vendedor> vendedores = deserializarVendedores("listaVendedores.ser");
        int numIniCompradores = compradores.size();
        int numIniVendedores = vendedores.size();
        
        try{
            valor = (String)cbxRegistro.getValue();
        } catch(Exception e) {
            e.getMessage();
            valor = "Sin valor";
        }
        
        switch (valor) {
            case "Vendedor":
                vendedores = modificarListaVendedor(vendedores, comprobarGmail(fldCorreo.getText()), fldNombre.getText(), fldApellido.getText(), fldCorreo.getText(), fldOrganizacion.getText(), fldUsuario.getText(), fldContra.getText());
                int numFinVendedores1 = vendedores.size();
                if (numIniVendedores == numFinVendedores1) {
                    retornoVentanaFXML();
                }
                else {
                    serializarVendedores(vendedores, "listaVendedores.ser");
                    retornoVentanaFXML();
                }
                break;
                
            case "Comprador":
                compradores = modificarListaComprador(compradores, comprobarGmail(fldCorreo.getText()), fldNombre.getText(), fldApellido.getText(), fldCorreo.getText(), fldOrganizacion.getText(), fldUsuario.getText(), fldContra.getText());
                int numFinCompradores1 = compradores.size();
                if (numIniCompradores == numFinCompradores1) {
                    retornoVentanaFXML();
                }
                else {
                    serializarCompradores(compradores, "listaCompradores.ser");
                    retornoVentanaFXML();
                }
                break;
                
            case "Vendedor y comprador":
                vendedores = modificarListaVendedor(vendedores, comprobarGmail(fldCorreo.getText()), fldNombre.getText(), fldApellido.getText(), fldCorreo.getText(), fldOrganizacion.getText(), fldUsuario.getText(), fldContra.getText());
                compradores = modificarListaComprador(compradores, comprobarGmail(fldCorreo.getText()), fldNombre.getText(), fldApellido.getText(), fldCorreo.getText(), fldOrganizacion.getText(), fldUsuario.getText(), fldContra.getText());
                int numFinVendedores2 = vendedores.size();
                int numFinCompradores2 = compradores.size();
                if ((numIniVendedores == numFinVendedores2) && (numIniCompradores == numFinCompradores2)) {
                    retornoVentanaFXML();
                }
                else {
                    serializarVendedores(vendedores, "listaVendedores.ser");
                    serializarCompradores(compradores, "listaCompradores.ser");
                    retornoVentanaFXML();
                }
                break;
                
            default:
                ;
                break;
        }
    }
    
    //Adicionales
    private static void retornoVentanaFXML() {
        try {
            App.setRoot("VentanaFXML");
        } catch (IOException ex) {
            Alert a = new Alert(Alert.AlertType.ERROR, "No es posible cambiar de menú");
        }   
    }
    
}
