    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Comprador;
import static ec.edu.espol.model.Comprador.deserializarCompradores;
import ec.edu.espol.model.Usuario;
import ec.edu.espol.model.Vendedor;
import static ec.edu.espol.model.Vendedor.deserializarVendedores;
import ec.edu.espol.model.Venta;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author gerar
 */
    public class VentanaCompradorFXMLController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    @FXML
    private Text rolText;
    @FXML
    private Text userText;
    @FXML
    private Text nombreText;
    @FXML
    private Text apellidoText;
    @FXML
    private Text correoText;
    @FXML
    private Text orgText;
    @FXML
    private PasswordField contField;
    @FXML
    private PasswordField contField2;
    @FXML
    private Button cambioBtn;
    @FXML
    private Button cambioBtn1;
    @FXML
    private ComboBox cbxRol;
    @FXML
    private ComboBox cbxFiltro;
    @FXML
    private TextField recoDesde;
    @FXML
    private TextField recoHasta;
    @FXML
    private TextField anioDesde;
    @FXML
    private TextField anioHasta;
    @FXML
    private TextField precioDesde;
    @FXML
    private TextField precioHasta;
    @FXML
    private HBox hBox;
    
    Comprador user = Comprador.deserializarUsuarioComprador("compradorLog.ser");
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        rolText.setText("Comprador");
        userText.setText(user.getNombreUsuario());
        nombreText.setText(user.getNombres());
        apellidoText.setText(user.getApellidos());
        correoText.setText(user.getCorreo());
        orgText.setText(user.getOrganizacion());
        ArrayList<String> lista = new ArrayList<>();
        lista.add("Comprador");
        lista.add("Vendedor");
        lista.add("Comprador y vendedor");
        cbxRol.setItems(FXCollections.observableArrayList(lista));
        ArrayList<String> listaTipos = new ArrayList<>();
        listaTipos.add("Carro");
        listaTipos.add("Camioneta");
        listaTipos.add("Camion");
        listaTipos.add("Motocicleta");
        cbxFiltro.setItems(FXCollections.observableArrayList(listaTipos));
        recoDesde.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("[0-9.]*")) ? change : null));
        recoHasta.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("[0-9.]*")) ? change : null));
        anioDesde.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("[0-9.]*")) ? change : null));
        anioHasta.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("[0-9.]*")) ? change : null));
        precioDesde.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("[0-9.]*")) ? change : null));
        precioHasta.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("[0-9.]*")) ? change : null));
    }
    public void cambiarContraseña(){
        String contActual = contField.getText();
        if(user.getClave().equals(contActual)){
            user.setClave(contField2.getText());
            Alert a = new Alert(Alert.AlertType.INFORMATION, "Contraseña cambiada correctamente");
            a.showAndWait();
        }else{
            Alert a = new Alert(Alert.AlertType.ERROR, "Contraseña incorrecta");
            a.showAndWait();
        }
    }
    
    public void iniciarCambioRol(){
        hBox.getChildren().remove(0);
        cbxRol.setDisable(false);
        cbxRol.setVisible(true);
        cambioBtn1.setDisable(true);
        
    }
    
    
    public void cambiarRol(){
        
        
    }
    public void iniciarBusqueda(){
        String tipo = (String) cbxFiltro.getValue();
        double recorridoDesde;
        try {
            recorridoDesde = Double.parseDouble(recoDesde.getText());
        } catch (NumberFormatException e) {
            recorridoDesde = 0.0;
        }
        double recorridoHasta;
        try {
            recorridoHasta = Double.parseDouble(recoHasta.getText());
        } catch (NumberFormatException e) {
            recorridoHasta = 0.0;
        }
        int yearDesde;
        try {
            yearDesde = Integer.parseInt(anioDesde.getText());
        } catch (NumberFormatException e) {
            yearDesde = 0;
        }
        int yearHasta;
        try {
            yearHasta = Integer.parseInt(anioHasta.getText());
        } catch (NumberFormatException e) {
            yearHasta = 0;
        }
        double costDesde;
        try {
            costDesde = Double.parseDouble(precioDesde.getText());
        } catch (NumberFormatException e) {
            costDesde = 0.0;
        }
        double costHasta;
        try {
            costHasta = Double.parseDouble(precioHasta.getText());
        } catch (NumberFormatException e) {
            costHasta = 0.0;
        }
        ArrayList<Venta> ventas = Venta.deserializarVentas("ventas.ser");
        Venta.filtrarVentas(ventas, tipo, recorridoDesde, recorridoHasta, yearDesde, yearHasta, recorridoDesde, recorridoHasta);
        try {
                App.setRoot("VentanaOfertarFXML");
            } catch (IOException ex) {
                Alert a = new Alert(Alert.AlertType.ERROR, "No es posible cambiar de menú");
            }
    }   
}
