/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author gerar
 */
public class VentanaFXMLController implements Initializable {

    @FXML
    private Button btnCambioIniSes;
    @FXML
    private Button btnCambioRegistro;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void cambiarInicioSesion(MouseEvent event) {
        try {
            App.setRoot("InicioSesionFXML");
        } catch (IOException ex) {
            Alert a = new Alert(Alert.AlertType.ERROR, "No es posible cambiar de menú");
        }
    }

    @FXML
    private void cambiarRegistro(MouseEvent event) {
        try {
            App.setRoot("RegistroFXML");
        } catch (IOException ex) {
            Alert a = new Alert(Alert.AlertType.ERROR, "No es posible cambiar de menú");
        }
    }
    
}
