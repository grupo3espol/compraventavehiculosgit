/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.Carro;
import ec.edu.espol.model.Comprador;
import ec.edu.espol.model.Oferta;
import ec.edu.espol.model.Vendedor;
import ec.edu.espol.model.Venta;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author andre
 */
public class VentanaOfertarFXMLController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    @FXML 
    private VBox vBox;
    @FXML
    private TextField txt;
    @FXML
    private ComboBox cbx;
    @FXML
    private Button btnOferta;
    @FXML
    private Button btnRegreso;
    Comprador comprador = Comprador.deserializarUsuarioComprador("usuarioLog.ser");
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ArrayList<Venta> ventas = Venta.deserializarVentas("ventasFiltradas.ser");
        ArrayList<Integer> numeros = new ArrayList<>();
        txt.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("[0-9.]*")) ? change : null));
        for(int i=0; i<ventas.size(); i++){
            int numero = i+1;
            vBox.getChildren().add(new Text(numero+"\n"+ventas.get(i).getVehiculo().toString()));
            numeros.add(numero);
        }
        cbx.setItems(FXCollections.observableArrayList(numeros));
    }    
    
    public void ofertar(){
        try {
            ArrayList<Venta> ventasFiltradas = Venta.deserializarVentas("ventasFiltradas.ser");
            int numero = 0;
            numero = (Integer)cbx.getValue();
            Venta venta = ventasFiltradas.get(numero);
            int montoOferta = 0;
            montoOferta = Integer.parseInt(txt.getText());
            Oferta o = new Oferta(comprador, venta);
            o.setMonto(montoOferta);
            venta.getOfertas().add(o);
        } catch (NumberFormatException e) {
            Alert a = new Alert(Alert.AlertType.ERROR, "Porfavor ingrese un monto");
            a.showAndWait();
        } catch (Exception e) {
            Alert a = new Alert(Alert.AlertType.ERROR, "Porfavor eliga un vehiculo");
            a.showAndWait();
        }
    }
    
}
