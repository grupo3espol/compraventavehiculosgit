/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author gerar
 */
public class VentanaVendedorFXMLController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    
    @FXML
    private void ingresarVehículo(ActionEvent event) {
        try {
            App.setRoot("IngresoVehículoFXML");
        } catch (IOException ex) {
            Alert a = new Alert(Alert.AlertType.NONE,"No se puede cambiara el root");
            a.show();
        }
    }
    
    @FXML
    private void aceptarOferta (ActionEvent event) {
        Alert a = new Alert(Alert.AlertType.INFORMATION, "Aceptar Oferta");
        a.show();
    }
    
    @FXML
    private void salir (ActionEvent event){
        try {
            App.setRoot("InicioSesionFXML");
        } catch (IOException ex) {
            Alert a = new Alert(Alert.AlertType.NONE,"No se puede cambiara el root");
            a.show();
        }
    }
}
