/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.util.Scanner;

/**
 *
 * @author andre
 */
public class Camion extends Vehiculo{
    private int vidrios;
    private String traccion;

    public Camion(String placa, String modelo, String tipoMotor, double recorrido, String color, double precio, String tipo, int vidrios, String traccion, int año) {
        super(placa, modelo, tipoMotor, recorrido, color, precio, tipo, año);
        this.vidrios = vidrios;
        this.traccion = traccion;
    }

    public Camion(String placa, String modelo, String tipoMotor, double recorrido, String color, double precio, String tipo, int vidrios, String traccion, int año, String img) {
        super(placa, modelo, tipoMotor, recorrido, color, precio, tipo, año, img);
        this.vidrios = vidrios;
        this.traccion = traccion;
    }

    public static Camion nuevoCamion(){
        Scanner sc = new Scanner(System.in);
        String placa;
        String modelo;
        String tipoMotor;
        double recorrido;
        String color;
        double precio;
        String tipo;
        int vidrios;
        String traccion;
        int año;
        System.out.println("Ingrese la placa");
        placa = sc.nextLine();
        System.out.println("Ingrese el modelo");
        modelo = sc.nextLine();
        System.out.println("Ingrese el tipo de motor");
        tipoMotor = sc.nextLine();
        System.out.println("Ingrese el recorrido");
        recorrido = sc.nextDouble();
        System.out.println("Ingrese el color");
        color = sc.nextLine();
        System.out.println("Ingrese el precio");
        precio = sc.nextDouble();
        tipo = "Carro";
        System.out.println("Ingrese el numero de vidrios");
        vidrios = sc.nextInt();
        System.out.println("Ingrese la traccion");
        traccion = sc.nextLine();
        System.out.println("Ingrese el año");
        año = sc.nextInt();
        Camion c = new Camion(placa, modelo, tipoMotor,recorrido, color, precio, tipo, vidrios, traccion, año);
        return c;
  }

    @Override
    public String toString() {
        return super.toString() + "\n VIDRIOS:  " + vidrios + "\n TRACCION:  " + traccion+ "\n --------------------------------------- \n";
    }
    
    
    
}
