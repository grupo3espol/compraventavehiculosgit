/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.util.Scanner;

/**
 *
 * @author andre
 */
public class Carro extends Vehiculo {
    private int vidrios;
    private String transmision;

    public Carro(String placa, String modelo, String tipoMotor, double recorrido, String color, double precio, String tipo, int vidrios, String transmision, int año) {
        super(placa, modelo, tipoMotor, recorrido, color, precio, tipo, año);
        this.vidrios = vidrios;
        this.transmision = transmision;
    }

    public Carro(String placa, String modelo, String tipoMotor, double recorrido, String color, double precio, String tipo, int vidrios, String transmision, int año, String img) {
        super(placa, modelo, tipoMotor, recorrido, color, precio, tipo, año, img);
        this.vidrios = vidrios;
        this.transmision = transmision;
    }
    
    
    
    public int getVidrios() {
        return vidrios;
    }

    public String getTransmision() {
        return transmision;
    }

    public void setVidrios(int vidrios) {
        this.vidrios = vidrios;
    }

    public void setTransmision(String transmision) {
        this.transmision = transmision;
    }

    @Override
    public String toString() {
        return super.toString() + "\n VIRIOS:  " + vidrios + "\n TRANSMISION: " + transmision+"\n --------------------------------------- \n";
    }
    
    
    public static Carro nuevoCarro(){
        Scanner sc = new Scanner(System.in);
        String placa;
        String modelo;
        String tipoMotor;
        double recorrido;
        String color;
        double precio;
        String tipo;
        int vidrios;
        int año;
        String transmision;
        System.out.println("Ingrese la placa");
        placa = sc.nextLine();
        System.out.println("Ingrese el modelo");
        modelo = sc.nextLine();
        System.out.println("Ingrese el tipo de motor");
        tipoMotor = sc.nextLine();
        System.out.println("Ingrese el recorrido");
        recorrido = sc.nextDouble();
        System.out.println("Ingrese el color");
        color = sc.nextLine();
        System.out.println("Ingrese el precio");
        precio = sc.nextDouble();
        tipo = "Carro";
        System.out.println("Ingrese el numero de vidrios");
        vidrios = sc.nextInt();
        System.out.println("Ingrese la transmision");
        transmision = sc.nextLine();
        System.out.println("Ingrese el año");
        año = sc.nextInt();
        Carro c = new Carro(placa, modelo, tipoMotor,recorrido, color, precio, tipo, vidrios, transmision, año);
        return c;
  }
    
}
