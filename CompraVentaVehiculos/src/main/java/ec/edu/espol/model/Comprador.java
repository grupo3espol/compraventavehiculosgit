/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;
import javafx.scene.control.Alert;
/**
 *
 * @author andre
 */
public class Comprador extends Usuario implements Serializable{
    private ArrayList<Venta> ventas;
    private static final long serialVersionUID = 177013;

    public Comprador(String nombres, String apellidos, String correo, String organizacion, String nombreUsuario, String clave) {
        super(nombres, apellidos, correo, organizacion, nombreUsuario, clave);
    }

    @Override
    public String toString() {
        return super.toString() + "lol"; //To change body of generated methods, choose Tools | Templates.
    }
    
    public boolean disponibilidad(ArrayList<Comprador> compradores){
        boolean disponible = true;
        for(Comprador c : compradores){
            if(this.correo.equals(c.correo) || this.nombreUsuario.equals(c.nombreUsuario)){
                disponible = false;
            }
        }
        return disponible;
    }
    public static Comprador comprobarExistencia(ArrayList<Comprador> compradores, String usuario, String clave){
        Comprador comp = null;
        for(Comprador c : compradores){
            if(c.nombreUsuario.equals(usuario)&& c.clave.equals(clave)){
                comp = c;
            }
        }
        return comp;
    }
    
    public static void serializarCompradores(ArrayList<Comprador> compradores, String nomfile) {
        try (FileOutputStream f = new FileOutputStream(nomfile); ObjectOutputStream out = new ObjectOutputStream(f)) {
            out.writeObject(compradores);
        } catch (Exception e) {
            e.getMessage();
        }
    }
    
    public static ArrayList<Comprador> deserializarCompradores(String nomfile) {
        try (FileInputStream f = new FileInputStream(nomfile); ObjectInputStream in = new ObjectInputStream(f)) {
            ArrayList<Comprador> compradores = (ArrayList<Comprador>)in.readObject();
            return compradores;
        } catch (Exception e) {
            e.getMessage();
        }
        return new ArrayList<>();
    }
    
    public static ArrayList<Comprador> modificarListaComprador(ArrayList<Comprador> compradores, boolean validacion, String fldNombre, String fldApellido, String fldCorreo, String fldOrganizacion, String fldUsuario, String fldContra) {
        if (validacion) {
            Comprador c = new Comprador(fldNombre, fldApellido, fldCorreo, fldOrganizacion, fldUsuario, fldContra);
            boolean existencia = c.disponibilidad(compradores);
            if (!existencia) {
                compradores.add(c);
                return compradores;
            }
            else {
                Alert a = new Alert(Alert.AlertType.ERROR, "El usuario y correo se encuentran en uso");
                return compradores;
            }      
        }   
        else {
            Alert a = new Alert(Alert.AlertType.ERROR, "Debe usar un correo de tipo Gmail");
            return compradores;
        }
    }
    public static void serializarUsuarioComprador(Comprador c, String nomfile) {
        try (FileOutputStream f =new FileOutputStream(nomfile); ObjectOutputStream out = new ObjectOutputStream(f)) {
            out.writeObject(c);
        } catch (Exception e) {
            e.getMessage();
        }
    }
      public static Comprador deserializarUsuarioComprador(String nomfile) {
        try (FileInputStream f = new FileInputStream(nomfile); ObjectInputStream in = new ObjectInputStream(f)) {
            Comprador c = (Comprador)in.readObject();
            return c;
        } catch (Exception e) {
            e.getMessage();
        }
        return null;
    }    

}
