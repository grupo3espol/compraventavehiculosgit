/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.util.Scanner;

/**
 *
 * @author andre
 */
public class Motocicleta extends Vehiculo{
    private String asiento;
    private String cadena;

    public Motocicleta(String placa, String modelo, String tipoMotor, double recorrido, String color, double precio, String tipo, String asiento, String cadena, int año) {
        super(placa, modelo, tipoMotor, recorrido, color, precio, tipo, año);
        this.asiento = asiento;
        this.cadena = cadena;
        this.año = año;
    }

    public Motocicleta(String placa, String modelo, String tipoMotor, double recorrido, String color, double precio, String tipo, String asiento, String cadena, int año, String img) {
        super(placa, modelo, tipoMotor, recorrido, color, precio, tipo, año, img);
        this.asiento = asiento;
        this.cadena = cadena;
    }
     
    @Override
    public String toString() {
        return super.toString() + "\n ASIENTO: " + asiento + "\n CADENA: " + cadena + "\n --------------------------------------- \n";
    }
    

    
    
    
        public static Motocicleta nuevaMoto(){
        Scanner sc = new Scanner(System.in);
        String placa;
        String modelo;
        String tipoMotor;
        double recorrido;
        String color;
        double precio;
        String tipo;
        String asiento;
        String cadena;
        int año;
        System.out.println("Ingrese la placa");
        placa = sc.nextLine();
        System.out.println("Ingrese el modelo");
        modelo = sc.nextLine();
        System.out.println("Ingrese el tipo de motor");
        tipoMotor = sc.nextLine();
        System.out.println("Ingrese el recorrido");
        recorrido = sc.nextDouble();
        System.out.println("Ingrese el color");
        color = sc.nextLine();
        System.out.println("Ingrese el precio");
        precio = sc.nextDouble();
        tipo = "Carro";
        System.out.println("Ingrese el tipo de asiento");
        asiento = sc.nextLine();
        System.out.println("Ingrese el tipo de cadena");
        cadena = sc.nextLine();
        System.out.println("Ingrese el año");
        año = sc.nextInt();
        Motocicleta m = new Motocicleta(placa, modelo, tipoMotor,recorrido, color, precio, tipo, asiento, cadena, año);
        return m;
  }
    
}
