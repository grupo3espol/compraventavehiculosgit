/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author andre
 */
public class Oferta {
    private Comprador comprador;
    private Venta venta;
    private double monto;

    public Oferta(Comprador comprador, Venta venta) {
        this.comprador = comprador;
        this.venta = venta;
        this.monto = 0;
    }

    @Override
    public String toString() {
        return this.comprador.correo +"\n" + this.monto;
    }
    
    
    
    public static Venta elegirVenta(ArrayList<Venta> ventas){
        Scanner sc = new Scanner(System.in);
        int indice = 0;
        int eleccion = 0;
        Venta v = null;
        do{
            System.out.print(ventas.get(indice));
            
            System.out.println("1. Siguiente ");
            if(indice > 0) {
                System.out.println("2. Anterior ");
            }
            System.out.println("3. Elegir");
            eleccion = sc.nextInt();
            switch (eleccion) {
                case 1:
                    indice++;
                    break;
                case 2:
                    indice--;
                    break;
                case 3:   
                    System.out.println("Este vehiculo ha sido seleccionado.");
                    v = ventas.get(indice);
                    break;
                default:
                    break;
            }
        }while(eleccion!= 3);
        return v;
    }
    
    public void realizarOferta(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese el monto a ofertar.");
        this.monto = sc.nextDouble();
        this.venta.getOfertas().add(this);
    }

    public double getMonto() {
        return monto;
    }

    public Venta getVenta() {
        return venta;
    }

    public void setComprador(Comprador comprador) {
        this.comprador = comprador;
    }

    public Comprador getComprador() {
        return comprador;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }
    
    
}
