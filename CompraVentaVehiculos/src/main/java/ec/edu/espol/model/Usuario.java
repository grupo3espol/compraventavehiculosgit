/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

/**
 *
 * @author andre
 */
public class Usuario implements Serializable{
    protected String nombres;
    protected String apellidos;
    protected String correo;
    protected String organizacion;  
    protected String nombreUsuario;
    protected String clave;
    protected String cedula;
    private static final long serialVersionUID = 8799656478674716638L;

    public Usuario(String nombres, String apellidos, String correo, String organizacion, String nombreUsuario, String clave) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.correo = correo;
        this.organizacion = organizacion;
        this.nombreUsuario = nombreUsuario;
        this.clave = clave;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getCorreo() {
        return correo;
    }

    public String getClave() {
        return clave;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public String getNombres() {
        return nombres;
    }

    public String getOrganizacion() {
        return organizacion;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public void setOrganizacion(String organizacion) {
        this.organizacion = organizacion;
    }

    @Override
    public boolean equals(Object o) {
 
        if(o==null){
            return false;
        }
        if (o.getClass()!=this.getClass()){
            return false;
        }
        if (o==this){
            return true;
        }
        
        Usuario u = (Usuario)o;
        return u.cedula.equals(this.cedula);
    }
    
    public static boolean comprobarGmail(String correo) { //El envío de mails solo funciona con cuentas gmail
        String[] partido = correo.split("@");
        if (partido[1].equals("gmail.com")) {
            return true;
        }
        else
            return false;
    }
    
    public static void serializarUsuarios(ArrayList<Usuario> usuarios, String nomfile) {
        try (FileOutputStream f =new FileOutputStream(nomfile); ObjectOutputStream out = new ObjectOutputStream(f)) {
            out.writeObject(usuarios);
        } catch (Exception e) {
            e.getMessage();
        }
    }
    
    public static ArrayList<Usuario> deserializarUsuarios(String nomfile) {
        try (FileInputStream f = new FileInputStream(nomfile); ObjectInputStream in = new ObjectInputStream(f)) {
            ArrayList<Usuario> usuarios = (ArrayList<Usuario>)in.readObject();
            return usuarios;
        } catch (Exception e) {
            e.getMessage();
        }
        return new ArrayList<>();
    }
    
    public static String encriptarClave(String clave) throws NoSuchAlgorithmException{
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(clave.getBytes());
        byte[] digest = md.digest();
        StringBuilder sb = new StringBuilder();
        for(byte b : digest){
            sb.append(String.format("%02x", b & 0xff));
        }
        return sb.toString();
    }
    

    
    
}
