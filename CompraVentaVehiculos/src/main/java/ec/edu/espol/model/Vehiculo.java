/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author andre
 */
public class Vehiculo implements Serializable{
    protected String placa;
    protected String modelo;
    protected String tipoMotor;
    protected double recorrido;
    protected String color;
    protected double precio;
    protected String tipo;
    protected int año;
    protected String img;
    private static final long serialVersionUID = 8799656478674716638L;

    public Vehiculo(String placa, String modelo, String tipoMotor, double recorrido, String color, double precio, String tipo, int año) {
        this.placa = placa;
        this.modelo = modelo;
        this.tipoMotor = tipoMotor;
        this.recorrido = recorrido;
        this.color = color;
        this.precio = precio;
        this.tipo = tipo;
        this.año = año;
    }

    public Vehiculo(String placa, String modelo, String tipoMotor, double recorrido, String color, double precio, String tipo, int año, String img) {
        this.placa = placa;
        this.modelo = modelo;
        this.tipoMotor = tipoMotor;
        this.recorrido = recorrido;
        this.color = color;
        this.precio = precio;
        this.tipo = tipo;
        this.año = año;
        this.img = img;
    }
    
    

    @Override
    public String toString() {
        return "PLACA:  " + placa + "\n MODELO:  " + modelo + "\n MOTOR:  =" + tipoMotor + "\n RECORRIDO:  " + recorrido + "\n COLOR:  " + color + "\n PRECIO:  " + precio + "\n TIPO:  " + tipo + "\n AÑO:  " + año;
    }

    @Override
    public boolean equals(Object o) {
        if(o==null){
            return false;
        }
        if (o.getClass()!=this.getClass()){
            return false;
        }
        if (o==this){
            return true;
        }
        
        Vehiculo v = (Vehiculo)o;
        return v.placa.equals(this.placa);
    }
    
    
    public boolean disponibilidad(ArrayList<Vehiculo> vehiculos){
        boolean disponible = true;
        for(Vehiculo vehiculo: vehiculos){
            if(this.equals(vehiculo)){
                disponible = false;
            }
        }
        return disponible;
    }

    public String getModelo() {
        return modelo;
    }

    public String getPlaca() {
        return placa;
    }

    public double getPrecio() {
        return precio;
    }

    public double getRecorrido() {
        return recorrido;
    }

    public String getTipoMotor() {
        return tipoMotor;
    }

    public void setTipoMotor(String tipoMotor) {
        this.tipoMotor = tipoMotor;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }
    
    

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
    
    
    
    public static void serializarVehiculos(ArrayList<Vehiculo> vehiculos, String nomfile) {
        try (FileOutputStream f =new FileOutputStream(nomfile); ObjectOutputStream out = new ObjectOutputStream(f)) {
            out.writeObject(vehiculos);
        } catch (Exception e) {
            e.getMessage();
        }
    }
    
    public static ArrayList<Vehiculo> deserializarVehiculos(String nomfile) {
        try (FileInputStream f = new FileInputStream(nomfile); ObjectInputStream in = new ObjectInputStream(f)) {
            ArrayList<Vehiculo> vehiculos = (ArrayList<Vehiculo>)in.readObject();
            return vehiculos;
        } catch (Exception e) {
            e.getMessage();
        }
        return new ArrayList<>();
    }
    

}
