/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import static ec.edu.espol.model.Usuario.comprobarGmail;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;
import javafx.scene.control.Alert;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import javax.mail.Session;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.Message;

/**
 *
 * @author andre
 */
public class Vendedor extends Usuario {
    private ArrayList<Venta> ventas;

    public Vendedor(String nombres, String apellidos, String correo, String organizacion, String nombreUsuario, String clave) {
        super(nombres, apellidos, correo, organizacion, nombreUsuario, clave);
    }
    
    public boolean disponibilidad(ArrayList<Vendedor> vendedores){
        boolean disponible = true;
        for(Vendedor v : vendedores){
            if(this.correo.equals(v.correo) || this.nombreUsuario.equals(v.nombreUsuario)){
                disponible = false;
            }
        }
        return disponible;
    }
    
    public static Vendedor comprobarExistencia(ArrayList<Vendedor> vendedores, String usuario, String clave){
        Vendedor vend = null;
        for(Vendedor v : vendedores){
            if(v.nombreUsuario.equals(usuario)&& v.clave.equals(clave)){
                vend = v;
            }
        }
        return vend;
    }
    
    public static void serializarVendedores(ArrayList<Vendedor> vendedores, String nomfile) {
        try (FileOutputStream f =new FileOutputStream(nomfile); ObjectOutputStream out = new ObjectOutputStream(f)) {
            out.writeObject(vendedores);
        } catch (Exception e) {
            e.getMessage();
        }
    }
    
    public static ArrayList<Vendedor> deserializarVendedores(String nomfile) {
        try (FileInputStream f = new FileInputStream(nomfile); ObjectInputStream in = new ObjectInputStream(f)) {
            ArrayList<Vendedor> vendedores = (ArrayList<Vendedor>)in.readObject();
            return vendedores;
        } catch (Exception e) {
            e.getMessage();
        }
        return new ArrayList<>();
    }
    
    public static ArrayList<Vendedor> modificarListaVendedor(ArrayList<Vendedor> vendedores, boolean validacion, String fldNombre, String fldApellido, String fldCorreo, String fldOrganizacion, String fldUsuario, String fldContra) {
        if (validacion) {
            Vendedor v = new Vendedor(fldNombre, fldApellido, fldCorreo, fldOrganizacion, fldUsuario, fldContra);
            boolean existencia = v.disponibilidad(vendedores);
            if (!existencia) {
                vendedores.add(v);
                return vendedores;
            }
            else {
                Alert a = new Alert(Alert.AlertType.ERROR, "El usuario y correo se encuentran en uso");
                return vendedores;
            }      
        }   
        else {
            Alert a = new Alert(Alert.AlertType.ERROR, "Debe usar un correo de tipo Gmail");
            return vendedores;
        }
    }
    
    //Envios de mensajes a correos 
    //Código de: https://www.campusmvp.es/recursos/post/como-enviar-correo-electronico-con-java-a-traves-de-gmail.aspx
    public void enviarMail(Comprador c, String clave) {
        Properties props = System.getProperties();
        props.put("mail.smtp.host", "smtp.gmail.com");  //El servidor SMTP de Google
        props.put("mail.smtp.user", this.correo);
        props.put("mail.smtp.clave", clave);    //La clave de la cuenta
        props.put("mail.smtp.auth", "true");    //Usar autenticación mediante usuario y clave
        props.put("mail.smtp.starttls.enable", "true"); //Para conectar de manera segura al servidor SMTP
        props.put("mail.smtp.port", "587"); //El puerto SMTP seguro de Google
        
        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);
    
        try {
            message.setFrom(new InternetAddress(this.correo));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(c.getCorreo()));   //Se podrían añadir varios de la misma manera
            message.setSubject("Oferta aceptada!");
            message.setText("Hola, "+c.getNombres()+" "+c.getApellidos()+"\nHe aceptado tu oferta, gracias por realizar la compra!"); //Cambiar cuerpo
            Transport transport = session.getTransport("smtp");
            transport.connect("smtp.gmail.com", this.correo, clave);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }
        catch (MessagingException me) {
            me.printStackTrace();   //Si se produce un error
        }
    }
    
    public static void serializarUsuarioVendedor(Vendedor v, String nomfile) {
        try (FileOutputStream f =new FileOutputStream(nomfile); ObjectOutputStream out = new ObjectOutputStream(f)) {
            out.writeObject(v);
        } catch (Exception e) {
            e.getMessage();
        }
    }
    
    public static Vendedor deserializarUsuarioVendedor(String nomfile) {
        try (FileInputStream f = new FileInputStream(nomfile); ObjectInputStream in = new ObjectInputStream(f)) {
            Vendedor v = (Vendedor)in.readObject();
            return v;
        } catch (Exception e) {
            e.getMessage();
        }
        return null;
    }
}
