/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

/**
 *
 * @author andre
 */
public class Venta {
    private String placaVehiculo;
    private Vehiculo vehiculo;
    private Vendedor vendedor;
    private double precio;
    private ArrayList<Oferta> ofertas;

    public Venta(Vehiculo vehiculo, Vendedor vendedor) {
        this.vehiculo = vehiculo;
        this.vendedor = vendedor;
        this.precio = vehiculo.precio;
        this.placaVehiculo = vehiculo.placa;
        this.ofertas = new ArrayList();
    }

    public ArrayList<Oferta> getOfertas() {
        return ofertas;
    }

    public void setOfertas(ArrayList<Oferta> ofertas) {
        this.ofertas = ofertas;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public String getPlacaVehiculo() {
        return placaVehiculo;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPlacaVehiculo(String placaVehiculo) {
        this.placaVehiculo = placaVehiculo;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }
    
    public Oferta revisarOfertas(){
        Scanner sc = new Scanner(System.in);
        int indice = 0;
        int eleccion = 0;
        Oferta o = null;
        do{
            System.out.print(this.ofertas.get(indice));
            System.out.println("1. Siguiente ");
            if(indice > 0) {
                System.out.println("2. Anterior ");
            }
            System.out.println("3. Elegir");
            eleccion = sc.nextInt();
            switch (eleccion) {
                case 1:
                    indice++;
                    break;
                case 2:
                    indice--;
                    break;
                case 3:   
                    System.out.println("Esta oferta ha sido aceptada.");
                    o = this.ofertas.get(indice);
                    break;
                default:
                    break;
            }
        }while(eleccion!= 3);
        return o;
    }
    
    public void aceptarOferta(Oferta o, ArrayList<Venta> ventas, ArrayList<Vehiculo> vehiculos){
        String correo = o.getComprador().getCorreo();
        //se envia el correo de aceptacion
        vehiculos.remove(this.vehiculo);
        ventas.remove(this);
    }
    
    public static Venta comprobarExistencia(ArrayList<Venta> ventas, String placa){
        Venta vent = null;
        for(Venta v : ventas){
            if(v.vehiculo.placa.equals(placa)){
                vent = v;
            }
        }
        return vent;
    }

    @Override
    public String toString() {
        return "Venta" + "placaVehiculo=" + placaVehiculo + ", vehiculo=" + vehiculo + ", precio=" + precio + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }   

    
    
    public static void filtrarVentas(ArrayList<Venta> ventas, String tipo, double recorridoDesde, double recorridoHasta, int anioDesde, int anioHasta,double precioDesde, double precioHasta){
        ArrayList<Venta> ventasFiltradas = new ArrayList<>();
        for(Venta venta : ventas){
            if((tipo.equals(venta.getVehiculo().getTipo())||tipo==null) && //Validacion tipo
               ((recorridoDesde==0 || recorridoDesde <= venta.getVehiculo().getRecorrido()) &&(recorridoHasta==0||recorridoHasta>=venta.getVehiculo().getRecorrido())) && //Validacion recorrido
                (anioDesde==0 || anioDesde<=venta.getVehiculo().getAño()) && (anioHasta==0 || anioHasta>= venta.getVehiculo().getAño()) &&//Validacion año
                (precioDesde==0 || precioDesde<=venta.getVehiculo().getPrecio()) && (precioHasta==0 || precioHasta>=venta.getVehiculo().getPrecio())) //Validacion precio
                ventasFiltradas.add(venta);
        }
        serializarVentas(ventasFiltradas, "ventasFiltradas.ser");
    }
    
    public static void serializarVentas(ArrayList<Venta> ventas, String nomfile) {
        try (FileOutputStream f =new FileOutputStream(nomfile); ObjectOutputStream out = new ObjectOutputStream(f)) {
            out.writeObject(ventas);
        } catch (Exception e) {
            e.getMessage();
        }
    }
    
    public static ArrayList<Venta> deserializarVentas(String nomfile) {
        try (FileInputStream f = new FileInputStream(nomfile); ObjectInputStream in = new ObjectInputStream(f)) {
            ArrayList<Venta> ventas = (ArrayList<Venta>)in.readObject();
            return ventas;
        } catch (Exception e) {
            e.getMessage();
        }
        return new ArrayList<>();
    }
    
    

    
    
}
